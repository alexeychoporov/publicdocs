# Biometric signature in MyIPCO.xixoio.com

We could use jQuery plugin below to get client's signature.

<https://github.com/brinley/jSignature/>

<https://github.com/willowsystems/jSignature> - older version

Then in backend we could apply signature image to generated pdf.

# Blockchain part

[optional] Sign pdf with our/referrer/referee digital signature.

Get hash of biometrically signed pdf.

Pavel suggested to create ethereum contract to store history of signed pdf's.

Push hash of pdf [and optional digital signatures public part] to ethereum network.